# sistema de estudiantes en C

## Integrantes:

* David Ezequien Wasyluk davidezequielwasyluk@gmail.com
* Liber Rossetti B. liberrb@gmail.com
* Juan Ribeiro ribeiro.jer@hotmail.com

## Implementacion:

* Crear y listar estudiantes
* Buscar estudiantes por nombre y apellido
* Buscar estudiantes por rango de edad
* Crear y listar materias
* Anotarse en N materias
* Rendir una materia

Extras:
* Generar estudiantes de prueba y materias aleatorias de forma masiva
* Cálculo de promedios (del estudiante)
* Archivo de configuración general donde se especifican las variables del sistema.
* Persistencia. (guardar resultados de listas de alumnos generadas y sus detalles)
* Tests.
* Integración continua. (depende de mantener los test al dia)

## Compilación y ejecución:

- [ ] Linux

Sistema Principal
```
cd {path_proyecto}
gcc src/main.c -o main && ./main
```
Tests
```
cd {path_proyecto}
gcc src/test.c -o test && ./test
```

- [ ] Windows

Sistema Principal
```
cd {path_proyecto}
gcc src/main.c -o main
./main.exe
```
Tests
```
cd {path_proyecto}
gcc src/test.c -o test
./test.exe
```


## Detalle de Implementacion:

- [ ] Test
Al ejecutar test nos devolvera un booleano (true o false) de cada prueba junto a su descripcion.
Luego de cada cambio se recomiendo probar el test para darse cuenta de una manera rapida si los cambios en cuestion probocaron una falla en el sistema.
Nuevas funcionalidades hay que agregarlas como casos de test.

```
### Running Tests ####
TestAgregarUnEstudiante: true 
TestAgregarEstudiantes: true 
TestAgregarUnaMateria: true 
TestAgregarMaterias: true 
TestAnotarAlumnoAMateria 1: true 
TestAnotarAlumnoAMateria 2: true 
TestAnotarAlumnoAMateria 3: true 
TestAnotarAlumnoAMateriaQueNoExiste: true 
Estudiante no encontrado.
TestAnotarAlumnoQueNoExisteAMateria: true 
TestRendirUnaMateria: true 
TestRendirUnaMateriaQueNoEstaAnotado: true 
TestEliminarEstudianteDeLaLista: true 
TestEliminarEstudianteQueNoExisteDeLaLista: true 
TestBuscarEstudiantePorApeNom: true 
TestBuscarEstudianteQueNoExistePorApeNom: true 
TestExportarListaEstudiantes: true 
TestExportarDetalleDeUnEstudiantes: true 
TestInicializarArchivoConfig: true 
```
- [ ] Aplicacion
Al ejecutar lo primero que nos aparecera es un menu con opciones:

```
ARCHIVO CONFIG LEIDO CON EXITO
MENU:
1. Crear/Iniciar Listas
2. Agregar nuevo Estudiante
3. Agregar nueva Materia
4. Anotar un Estudiante a una Materia
5. Rendir una Materia como Estudiante
6. Eliminar un Estudiante de la lista
7. Imprimir lista de Estudiantes
8. Imprimir lista de Materias
9. Buscar estudiante por nombre y apellido
10. Buscar estudiantes por rango de edad
11. Exportar lista de Estudiantes
12. Expotar detalle de Estudiante
0. Salir
```
Donde la primera linea nos indicara si se pudo leer con exito el archivo de configuracion de varialbes de entorno o feature flags
sus posibles respuestas: 
  * ARCHIVO CONFIG LEIDO CON EXITO : como bien india el nombre fue leido y seteadas sus valores con exito
  * ARCHIVO CONFIG NO LEIDO ESTA OPERANDO CON VALORES DEFAULT: no pudo ser leido el archivo, pero setea valor por defecto para poder seguir operando en la aplicacion

1. Crear/Iniciar Listas: utiliza valores del archivo de configuracion ( src/config.cfg ) para cargar de forma masiva y aleatoria alumnos y materias, como tambien la cantidad de cada uno
  
Primera ejecucion
```
INCIALIZACION COMPLETADA
```
Si se reinitenta inicializar
```
LA INCICIALIZACION YA HABIA SIDO EJECUTADA
```
Archivo Config
```
#lectura de valor true, para detectar si la carga fue exitosa
IS_OK=1

#nota minima para aprobar
NOTA=6

#cantidad de alumnos a generar
CANTIDAD_ALUMNOS_GENERAR=30

#cantidad de alumnos a generar
CANTIDAD_MATERIAS_GENERAR=10

#nombres para alumnos (minimo 7 nombres, repetirlos de ser necesario)
NOMBRE_ALUMNO=juan_pablo_pedro_analia_samantha_ramon_ximena_zara

#apellidos para alumnos (minimo 7 apellidos, repetirlos de ser necesario)
APELLIDO_ALUMNO=suarez_quintana_perez_rodriguez_alvarez_rial_gutierrez

#materias (minimo 7 nombres, repetirlos de ser necesario)
NOMBRE_MATERIA=analisis_ingles_algoritmos_discretas_estructuras_base De Datos_arquitectura
```

2. Agregar nuevo Estudiante: solicita los datos de un estudiante (nombre, apellido, fecha nacimiento) para añadirlo al listado de estudiantes, automaticamente se genera un legajo que es unico e irrepetible, que utilizaremos como id para futuras busquedas o asignaciones

```
Ingrese el nombre del estudiante (max. 20 caracteres):Juan Carlos

Ingrese el apellido del estudiante (max. 20 caracteres): Godines 

Se solicita la fecha de nacimiento del estudiante en formato aaaa/mm/dd: 1991/04/12
```

3. Agregar nueva Materia: solicita el nombre de la materia para añadirla al listado de materias, automaticamente se genera un ID de materia que unico e irrepetible, lo usaremos como id para futuras busquedas o asignaciones

```
Ingrese el nombre de la materia (max. 25 caracteres): Algoritmos y Programacion III
```

4. Anotar un Estudiante a una Materia: nos solicitara buscar por legajo un estudiante, luego nos mostrara materias en la que esta inscripto y materias disponibles de seleccion, se ingresa el id de materia y el estudiante queda inscripto con una nota por default 0

```
Ingrese el legajo del estudiante: 10010

Materia: ID: 1, Nombre: base De Datos

Materia: ID: 2, Nombre: estructuras

Materia: ID: 3, Nombre: algoritmos

Materia: ID: 4, Nombre: ingles

Materia: ID: 5, Nombre: discretas

Materia: ID: 6, Nombre: algoritmos

Seleccione una materia: 1

Inscripto en las siguientes materias:

Materia - ID: 1, Descripcion: base De Datos, Nota: 0
```

5. Rendir una Materia como Estudiante: nos solicitara buscar por legajo un estudiante, luego nos mostrara en las materias que esta inscripto, se selecciona una luego se ingresara la nota

```
Ingrese el legajo del estudiante: 10010
Inscripto en las siguientes materias:

Materia - ID: 1, Descripcion: base De Datos, Nota: 0

Seleccione una materia: 1
Se selecciono la materia: 
Materia - ID: 1, Descripcion: base De Datos, Nota: 0
Ingrese la nota:8
```

6. Eliminar un Estudiante de la lista: nos solicitara buscar por legajo un estudiante y lo eliminara de la lista de estudiantes.

```
Ingrese el legajo del estudiante: 10020
El estudiante con legajo 10020 fue eliminado con exito
```

7. Imprimir lista de Estudiantes: imprime la lista de todos los estudiantes (legajo, nombre, apellido, fecha nacimiento)

```
Estudiante: Legajo: 10001 Apellido: alvarez. Nombre: analia. Fecha de nacimiento: 1990/9/25
Estudiante: Legajo: 10004 Apellido: alvarez. Nombre: juan. Fecha de nacimiento: 1993/5/28
Estudiante: Legajo: 10015 Apellido: alvarez. Nombre: ramon. Fecha de nacimiento: 2001/3/13
Estudiante: Legajo: 10003 Apellido: alvarez. Nombre: samantha. Fecha de nacimiento: 1985/12/24
Estudiante: Legajo: 10023 Apellido: alvarez. Nombre: ximena. Fecha de nacimiento: 1988/6/16
```

8. Imprimir lista de Materias: imprime la lista de todas las materias (id materia y nombre)

```
Materia: ID: 1, Nombre: arquitectura
Materia: ID: 2, Nombre: algoritmos
Materia: ID: 3, Nombre: discretas
Materia: ID: 4, Nombre: base De Datos
Materia: ID: 5, Nombre: algoritmos
```

9. Buscar estudiante por nombre y apellido: se ingresa un nombre y un apellido y devolvera estudiantes que coincidan con los valores ingresados

```
Ingrese nombre: zara
Ingrese apellido del alumno: suarez

Estudiante: Legajo: 10093 Apellido: suarez. Nombre: zara. Fecha de nacimiento: 2001/4/10
Estudiante: Legajo: 10115 Apellido: suarez. Nombre: zara. Fecha de nacimiento: 1999/5/6
Estudiante: Legajo: 10166 Apellido: suarez. Nombre: zara. Fecha de nacimiento: 1983/12/13
Estudiante: Legajo: 10175 Apellido: suarez. Nombre: zara. Fecha de nacimiento: 1996/1/14
Estudiante: Legajo: 10229 Apellido: suarez. Nombre: zara. Fecha de nacimiento: 1980/12/23
Estudiante: Legajo: 10445 Apellido: suarez. Nombre: zara. Fecha de nacimiento: 1996/11/10
```

10. Buscar estudiantes por rango de edad: nos solicitara ingresar dos valores desde y hasta (edad minima y maximo) nos devolvera todos los estudiantes que coincidan con el rango ingresado

```
Ingrese el valor minimo de edad a buscar: 25
Ingrese el valor Maximo de edad a buscar: 37

        Estudiantes entre 25 y 37 anios de edad.
Estudiante: Legajo: 10014 Apellido: quintana. Nombre: pablo. Edad: 25
Estudiante: Legajo: 10027 Apellido: gutierrez. Nombre: pablo. Edad: 26
Estudiante: Legajo: 10011 Apellido: rodriguez. Nombre: juan. Edad: 27
Estudiante: Legajo: 10012 Apellido: rodriguez. Nombre: ximena. Edad: 27
Estudiante: Legajo: 10013 Apellido: gutierrez. Nombre: pedro. Edad: 27
Estudiante: Legajo: 10002 Apellido: quintana. Nombre: analia. Edad: 28
Estudiante: Legajo: 10019 Apellido: rodriguez. Nombre: juan. Edad: 29
Estudiante: Legajo: 10003 Apellido: perez. Nombre: pablo. Edad: 31
Estudiante: Legajo: 10004 Apellido: gutierrez. Nombre: zara. Edad: 31
Estudiante: Legajo: 10009 Apellido: gutierrez. Nombre: pedro. Edad: 32
Estudiante: Legajo: 10017 Apellido: perez. Nombre: analia. Edad: 32
Estudiante: Legajo: 10028 Apellido: suarez. Nombre: zara. Edad: 32
Estudiante: Legajo: 10001 Apellido: suarez. Nombre: samantha. Edad: 33
Estudiante: Legajo: 10024 Apellido: perez. Nombre: analia. Edad: 33
Estudiante: Legajo: 10006 Apellido: rodriguez. Nombre: pedro. Edad: 34
Estudiante: Legajo: 10005 Apellido: rial. Nombre: pedro. Edad: 35
Estudiante: Legajo: 10022 Apellido: suarez. Nombre: pablo. Edad: 35
Estudiante: Legajo: 10026 Apellido: rodriguez. Nombre: analia. Edad: 35
```

11. Exportar lista de Estudiantes: exporta a un archivo en formato csv el listado de estudiantes (Estudiantes.csv)

```
Archivo generado: Estudiantes.csv
```

12. Expotar detalle de Estudiante: nos solicita un legajo y exporta a un archivo csv el detalle de dicho estudiantes (nombre, apellido, fecha nacimiento, materias inscripto y nota) (ejemplo: Estudiante_10002.csv)

```
Ingrese el legajo del estudiante: 10022
```

0. Salir: realiza la salida del sistema limpiando de memoria todos los struct iniciados con malloc


## Detalles, Aclaraciones y Limitantes:

* Se pobro ingresando de forma masiva distintas cantidades de estudiantes / materias y se detecto que hay un array que no pudimos declararlo con un tamaño dinamo, por lo que si se inicializan mas de 5000 alumnos se proboca un memory leak e inconsistencias. sin la funcionalidad que provee ese array se probo hasta 10000 alumnos ingresados de forma masiva sin problemas. por tal motivo se recomienda trabajar con una cantidad a los 5000 mencionados.
* Nombre, Apellido y Nombre Materia, permiten ingresar caracteres alfanumericos (no se valido que sea solo letras)
* El sistema no recupera datos almacenados en archivos o base de datos, sino que en cada ejecucion se puede generar automaticamente alumnos y materias.
  si durante la ejecucion del mismo mantiene en memoria todos los datos que se puedan ir generando (salvo que se solicite una limpieza de memoria de alguna lista en particular), 
* el sistema permite exportar a formato csv el listado de alumnos y el detalle de cada uno de ellos.


## Requerimientos:

Desarrollar un sistema de estudiantes en C. Debe permitir:


* Crear y listar estudiantes

* Buscar estudiantes por nombre

* Buscar estudiantes por rango de edad


Además cada estudiante puede anotarse en N materias.

* Crear y listar materias

* Anotarse en una materia

* Rendir una materia


El sistema debe poder soportar un gran listado de estudiantes y materias. Utilizar estructuras de datos para almacenar los listados. Decidir si se utiliza ordenamiento de los datos.


El diseño del sistema y los modelos es libre. Utilizar la creatividad para que el manejo del sistema sea lo más práctico posible. Si los requerimientos planteados son cumplidos, el ejercicio está aprobado. Puede pasar que en situaciones en la que la cantidad de datos sea muy grande, el sistema sea inmanejable. En ese caso, detallar las limitaciones de la solución propuesta (si las limitaciones son reconocidas no se considera desaprobado el punto).


Ideas no-obligatorias de implementar pero que suman puntos:
* Utilizar paginado
* Poder elegir el estudiante/materia de un listado reducido
* Generar estudiantes de prueba y materias aleatorias de forma masiva
* Estadísticas de los estudiantes y materias, etc.
* Árboles de correlatividad de materias (2).
  * ¿Qué pasa si una materia anterior está desaprobada? ¿Puede anotarse?
* Cálculo de promedios.
* Archivo de configuración general donde se especifican las variables del sistema.
* Persistencia (2).
* Validaciones complejas.
* Tests.
* Integración continua.


En el repositorio hacer un README con los integrantes, las consignas implementadas y los puntos extras que hayan desarrollado.


Grabar un video de máximo 10 minutos (puede ser menos). En el caso de grupo de a dos, intenten participar los dos, pueden enviar dos videos de más o menos la misma duración (5 minutos máximo aproximadamente).


Fecha de entrega: 2022-06-18

Sería enviar los videos si no los enviaron ya.


