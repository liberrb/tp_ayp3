#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

/*
PARA CORRERLO EN LINUX: 
gcc LinkedLHelper.c MenuLinkedList.c -o menuLista && ./menuLista

PARA CORRERLO EN WINDOWS: 
gcc LinkedLHelper.c MenuLinkedList.c -o menuLista 
./menuLista
*/

typedef struct sNodo
{
    int valor;
    struct sNodo *next;
} sNodo;

sNodo *head;
int solicitarUnValorInt();
void iniciarLista();
sNodo* obtenerPosicionParaInsertar(sNodo* nodo, int valor);
void insertar(int valor);
void imprimirNodo(sNodo *nodo);
void imprimir(sNodo *lista);
void limpiarMemoria(sNodo *lista);
bool buscarPorValor(sNodo *lista, int valor);
bool eliminarPorValor(int valor);
sNodo* obtenerPosicionParaEliminar(sNodo* nodo, int valor);
void eliminarValores(sNodo *nodo, int valor);
int tamanio(sNodo *lista);
sNodo* buscarPorIndice(sNodo *lista, int indice);
int validarTipoDatoInt();
bool eliminarPorIndice(int indice);


int main()
{
    int opcion;

    while (opcion != 9)
    {
        printf("\nMENU:\n");
        printf("1. Crear/Iniciar Lista\n");
        printf("2. Agregar Elemento\n");
        printf("3. Obtener tamanio\n");
        printf("4. Imprimir\n");
        printf("5. Obtener Elemento (por valor)\n");
        printf("6. Obtener Elemento (por indice)\n");
        printf("7. Eliminar Elemento (por valor)\n");//
        printf("8. Eliminar Elemento (por indice)\n");
        printf("9. Salir\n");
        printf("\nSeleccione una opcion: ");
        opcion = validarTipoDatoInt();

        switch (opcion)
        {
        case 1:
            printf("\n");
            iniciarLista();
            break;
        case 2:
            insertar(solicitarUnValorInt());
            break;
        case 3:
            printf("\nLa cantidad de elementos de la lista es: %d\n", tamanio(head));
            break;
        case 4:
            printf("\n");
            imprimir(head);
            break;
         case 5:
            printf("\n");
            buscarPorValor(head, solicitarUnValorInt());
            break;
        case 6:
            buscarPorIndice(head, solicitarUnValorInt());
            break;
        case 7:
            printf("\n");
            eliminarPorValor(solicitarUnValorInt());
            break;
        case 8:
            printf("\n");
            eliminarPorIndice(solicitarUnValorInt());
            break;
        case 9:
            limpiarMemoria(head);
            printf("Saliendo.\n");
            sleep(1);
            printf("Saliendo..\n");
            sleep(1);
            printf("Saliendo...\n");
            sleep(1);

            #if _WIN32
                system("cls");
            #else
                system("clear");
            #endif

            break;
        default:
            printf("###########\n");
            printf("numero de opcion incorrecto\n");
            printf("###########\n");
            break;
        }
    }
    return 0;
}

int validarTipoDatoInt()
{
    int opcion;
    while(scanf("%d",&opcion) != 1)
    {
        printf("Solo se aceptan numeros enteros, intentemos otra vez: ");
        while(getchar() != '\n');
    }
    return opcion;
}