# tp_ayp
Integrantes:
+ David Ezequien Wasyluk davidezequielwasyluk@gmail.com
+ Liber Rossetti B. liberrb@gmail.com
+ Juan Ribeiro ribeiro.jer@hotmail.com

Consigna del TP final:


Desarrollar un sistema de estudiantes en C. Debe permitir:


* Crear y listar estudiantes

* Buscar estudiantes por nombre

* Buscar estudiantes por rango de edad


Además cada estudiante puede anotarse en N materias.

* Crear y listar materias

* Anotarse en una materia

* Rendir una materia


El sistema debe poder soportar un gran listado de estudiantes y materias. Utilizar estructuras de datos para almacenar los listados. Decidir si se utiliza ordenamiento de los datos.


El diseño del sistema y los modelos es libre. Utilizar la creatividad para que el manejo del sistema sea lo más práctico posible. Si los requerimientos planteados son cumplidos, el ejercicio está aprobado. Puede pasar que en situaciones en la que la cantidad de datos sea muy grande, el sistema sea inmanejable. En ese caso, detallar las limitaciones de la solución propuesta (si las limitaciones son reconocidas no se considera desaprobado el punto).


Ideas no-obligatorias de implementar pero que suman puntos:
* Utilizar paginado
* Poder elegir el estudiante/materia de un listado reducido
* Generar estudiantes de prueba y materias aleatorias de forma masiva
* Estadísticas de los estudiantes y materias, etc.
* Árboles de correlatividad de materias (2).
  * ¿Qué pasa si una materia anterior está desaprobada? ¿Puede anotarse?
* Cálculo de promedios.
* Archivo de configuración general donde se especifican las variables del sistema.
* Persistencia (2).
* Validaciones complejas.
* Tests.
* Integración continua.


En el repositorio hacer un README con los integrantes, las consignas implementadas y los puntos extras que hayan desarrollado.


Grabar un video de máximo 10 minutos (puede ser menos). En el caso de grupo de a dos, intenten participar los dos, pueden enviar dos videos de más o menos la misma duración (5 minutos máximo aproximadamente).


Fecha de entrega: 2022-06-18

Sería enviar los videos si no los enviaron ya.
