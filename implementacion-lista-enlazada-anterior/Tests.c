#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

//
/*
PARA CORRERLO EN LINUX:
gcc LinkedLHelper.c Tests.c -o tests && ./tests

PARA CORRERLO EN WINDOWS:
gcc LinkedLHelper.c Tests.c -o tests
./tests
*/

typedef struct sNodo
{
    int valor;
    struct sNodo *next;
} sNodo;

sNodo *head;
int solicitarUnValorInt();
void iniciarLista();
sNodo *obtenerPosicionParaInsertar(sNodo *nodo, int valor);
void insertar(int valor);
void imprimirNodo(sNodo *nodo);
void imprimir(sNodo *lista);
void limpiarMemoria(sNodo *lista);
bool buscarPorValor(sNodo *lista, int valor);
bool eliminarPorValor(int valor);
sNodo *obtenerPosicionParaEliminar(sNodo *nodo, int valor);
void eliminarValores(sNodo *nodo, int valor);
int tamanio(sNodo *lista);
sNodo *buscarPorIndice(sNodo *lista, int indice);
bool eliminarPorIndice(int indice);

bool testAgregaElementosALista()
{
    insertar(2);
    insertar(66);
    insertar(66);
    insertar(3);
    insertar(8);
    insertar(66);
    return tamanio(head) >= 1;
}

bool testBuscarPorUnValor()
{
    return buscarPorValor(head, 3);
}

bool testBuscarPorUnValorNoInsertado()
{
    return !buscarPorValor(head, 99);
}

bool testBuscarPorUnValorRepetidoEnLista()
{
    return buscarPorValor(head, 66);
}

bool testBuscarPorIndiceMayorATamanioLista()
{
    return buscarPorIndice(head, 100) == NULL;
}

bool testLimpiarLista()
{
    limpiarMemoria(head);
    return head == NULL;
}

bool testBuscarPorIndiceListaVacia()
{
    return buscarPorIndice(head, 1) == NULL;
}

bool testBuscarPorIndice()
{
    insertar(2); // index 0
    insertar(3);
    insertar(8);
    insertar(66); // index 3
    insertar(66);
    insertar(67);
    insertar(68); // index 6
    sNodo *temp = buscarPorIndice(head, 0);
    return temp->valor == 2;
}

bool testEliminarPorIndice()
{
    eliminarPorIndice(1);
    sNodo *temp = buscarPorIndice(head, 5);
    return temp->valor == 68;
}

bool testEliminarPorValorInexistente()
{
    return !eliminarPorValor(100);
}

bool testEliminarPorValorExistenteRepetido()
{
    insertar(55);
    insertar(55);
    insertar(56);
    insertar(2);
    insertar(2);
    insertar(2);
    bool isOk = eliminarPorValor(68);
    imprimir(head);
    return isOk;
}

int main()
{
    printf("\n");
    printf("\nRunning Tests\n");
    printf("\n");
    printf("!!!testAgregaElementosALista %d \n", testAgregaElementosALista());
    printf("\n");
    printf("!!!testBuscarPorUnValor %d \n", testBuscarPorUnValor());
    printf("\n");
    printf("!!!testBuscarPorUnValorNoInsertado %d \n", testBuscarPorUnValorNoInsertado());
    printf("\n");
    printf("!!!testBuscarPorUnValorRepetidoEnLista %d \n", testBuscarPorUnValorRepetidoEnLista());
    printf("\n");
    printf("!!!testBuscarPorIndiceMayorATamanioLista %d \n", testBuscarPorIndiceMayorATamanioLista());
    printf("\n");
    printf("!!!testLimpiarLista %d \n", testLimpiarLista());
    printf("\n");
    printf("!!!testBuscarPorIndiceListaVacia %d \n", testBuscarPorIndiceListaVacia());
    printf("\n");
    printf("!!!testBuscarPorIndice %d \n", testBuscarPorIndice());
    printf("\n");
    printf("!!!testEliminarPorIndice %d \n", testEliminarPorIndice());
    printf("\n");
    printf("!!!testEliminarPorValorInexistente %d \n", testEliminarPorValorInexistente());
    printf("\n");
    printf("!!!testEliminarPorValorExistenteRepetido %d \n", testEliminarPorValorExistenteRepetido());
    return 0;
}