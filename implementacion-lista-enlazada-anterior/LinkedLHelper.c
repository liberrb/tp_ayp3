#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

typedef struct sNodo
{
    int valor;
    struct sNodo *next;
} sNodo;

sNodo *head = NULL;

sNodo *getNuevoNodo(int valor)
{
    sNodo *nuevoNodo = NULL;
    nuevoNodo = malloc(sizeof(struct sNodo));
    if (nuevoNodo != NULL)
    {
        nuevoNodo->next = NULL;
        nuevoNodo->valor = valor;
    }
    else
    {
        printf("ERROR MALLOC\n");
    }
    return nuevoNodo;
}

int solicitarUnValorInt();
void iniciarLista();
sNodo *obtenerPosicionParaInsertar(sNodo *nodo, int valor);
void insertar(int valor);
void imprimirNodo(sNodo *nodo);
void imprimir(sNodo *lista);
void limpiarMemoria(sNodo *lista);
bool buscarPorValor(sNodo *lista, int valor);
bool eliminarPorValor(int valor);
sNodo *obtenerPosicionParaEliminar(sNodo *nodo, int valor);
void eliminarValores(sNodo *nodo, int valor);
int tamanio(sNodo *lista);
sNodo *buscarPorIndice(sNodo *lista, int indice);
bool eliminarPorIndice(int indice);

int solicitarUnValorInt()
{
    int value;
    printf("ingrese un valor: ");
    while (scanf("%d", &value) != 1)
    {
        printf("Solo se aceptan numeros enteros, intentemos otra vez: ");
        while (getchar() != '\n')
            ;
    }
    return value;
}

void iniciarLista()
{
    if (head == NULL)
    {
        printf("la lista esta esperando que le agreguen elementos");
    }
    else
    {
        printf("\nla lista ya tiene elementos, ingrese: \n1 para limpiar la lista y volver a alimentarla. \n0 para cancelar la operacion.\n");
        int eleccion = solicitarUnValorInt();
        if (eleccion)
        {
            limpiarMemoria(head);
        }
        else
        {
            printf("continua trabajando con la lista actual");
        }
    }
}

sNodo *obtenerPosicionParaInsertar(sNodo *nodo, int valor)
{
    if (nodo->next == NULL || nodo->next->valor >= valor)
    {
        return nodo;
    }
    return obtenerPosicionParaInsertar(nodo->next, valor);
}

void insertar(int valor)
{
    if (head == NULL)
    {
        head = getNuevoNodo(valor);
    }
    else
    {
        sNodo *nuevoNodo = getNuevoNodo(valor);
        sNodo *nodoAux;

        if (head->valor > valor)
        {
            nodoAux = head;

            head = nuevoNodo;
            head->next = nodoAux;
        }
        else
        {
            sNodo *nodoPosicion = obtenerPosicionParaInsertar(head, valor);
            nodoAux = nodoPosicion->next;
            nuevoNodo = getNuevoNodo(valor);

            nodoPosicion->next = nuevoNodo;
            nuevoNodo->next = nodoAux;
        }
    }
}

bool eliminarPorValor(int valor)
{
    // Caso Lista vacia
    if (head == NULL)
    {
        printf("\nLa lista no contiene elementos.\n");
        return false;
    }

    // Caso valor esta en la cabeza (unica vez o repetido varias veces)
    else if (head->valor == valor)
    {
        while (head->valor == valor)
        {
            sNodo *aux = head;

            // Si cabeza es el unico elemento en lista
            if (head->next == NULL)
            {
                free(aux);
                head = NULL;
                break;
            }
            // Sino continuar
            else
            {
                head = head->next;
                free(aux);
            }
        }
        return true;
    }

    // Buscar el valor en los demas nodos
    else
    {
        sNodo *puntero = head;
        while (puntero != NULL && puntero->next != NULL)
        {
            if (puntero->next->valor == valor)
            {
                eliminarValores(puntero, valor);
                return true;
            }
            else
            {
                puntero = puntero->next;
            }
        }
        printf("\nValor no encontrado\n");
        return false;
    }
}

sNodo *obtenerPosicionParaEliminar(sNodo *nodo, int valor)
{
    if (nodo->next == NULL || nodo->next->valor == valor)
    {
        return nodo;
    }
    return obtenerPosicionParaEliminar(nodo->next, valor);
}

void eliminarValores(sNodo *pNodo, int valor)
{
    while (pNodo->next != NULL && pNodo->next->valor == valor)
    {
        sNodo *temp = pNodo->next;
        pNodo->next = pNodo->next->next;
        free(temp);
    }
}

void imprimirNodo(sNodo *nodo)
{
    if (nodo == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("valor: %d, nodoAlloc: %p, nodoNext: %p  \n", nodo->valor, nodo, nodo->next);
    }
}

void imprimir(sNodo *lista)
{
    sNodo *l;
    l = lista;
    if (l == NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while (l)
        {
            imprimirNodo(l);
            l = l->next;
        }
    }
}

void limpiarMemoria(sNodo *lista)
{
    struct sNodo *next;
    while (lista)
    {
        next = lista->next;
        free(lista);
        lista = next;
    }

    head = NULL;
    printf("Memoria Liberada!!!\n");
}

bool buscarPorValor(sNodo *lista, int valor)
{
    if (lista == NULL)
    {
        printf("Valor no encontrado\n");
        return false;
    }

    if (lista->valor == valor)
    {
        imprimirNodo(lista);

        if (!(lista->next != NULL && lista->next->valor == valor))
        {
            return true;
        }
    }

    return buscarPorValor(lista->next, valor);
}

int tamanio(sNodo *lista)
{
    if (lista == NULL)
    {
        return 0;
    }
    else
    {
        return 1 + tamanio(lista->next);
    }
}

sNodo *buscarPorIndice(sNodo *lista, int indice)
{

    if (lista == NULL)
    {
        printf("la lista se encuentra vacia!\n");
        return NULL;
    }

    int tam = tamanio(lista);

    if (tam < indice)
    {
        printf("la lista es menor al index seleccionado (%d) , el tamanio de la lista es %d \n", indice, tam);
        return NULL;
    }

    struct sNodo *aux;
    aux = lista;
    for (size_t i = 0; i < indice; i++)
    {
        aux = aux->next;
    }
    imprimirNodo(aux);
    return aux;
}

bool eliminarPorIndice(int indice)
{
    struct sNodo *temp = head;

    if (indice == 0)
    {
        head = head->next;
        temp->next = NULL;
        free(temp);
    }
    else
    {
        int tam = tamanio(head);
        if (tam < indice)
        {
            printf("la lista es menor al index seleccionado (%d) , el tamanio de la lista es %d \n", indice, tam);
            return 0;
        }

        for (int i = 0; i < indice - 1; i++)
        {
            temp = temp->next;
        }
        struct sNodo *delete = temp->next;
        temp->next = temp->next->next;
        delete->next = NULL;
        free(delete);
    }
    return 1;
}
