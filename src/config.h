#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAXLEN 200
#define CONFIG_FILE "src/config.cfg"
#define CONFIG_FILE_2 "config.cfg"

struct parametros
{
    char IS_OK[MAXLEN];
    char NOTA[MAXLEN];
    char CANTIDAD_ALUMNOS_GENERAR[MAXLEN];
    char CANTIDAD_MATERIAS_GENERAR[MAXLEN];
    char NOMBRE_ALUMNO[MAXLEN];
    char APELLIDO_ALUMNO[MAXLEN];
    char NOMBRE_MATERIA[MAXLEN];
} parametros;

//inicializar los parametros
void initParametros(struct parametros *params)
{
    strncpy(params->IS_OK, "0", MAXLEN);
    strncpy(params->NOTA, "100", MAXLEN);
    strncpy(params->CANTIDAD_ALUMNOS_GENERAR, "10", MAXLEN);
    strncpy(params->CANTIDAD_MATERIAS_GENERAR, "5", MAXLEN);
    strncpy(params->NOMBRE_ALUMNO, "juan_pablo_pedro_analia_samantha_ramon_ximena_zara", MAXLEN);
    strncpy(params->APELLIDO_ALUMNO, "suarez_quintana_perez_rodriguez_alvarez_rial_gutierrez", MAXLEN);
    strncpy(params->NOMBRE_MATERIA, "Analisis Matematico I_Ingles Basico_Algoritmos_Discretas_Estructura de Datos_Base De Datos_Arquitectura", MAXLEN);
    
}

char *trim(char *s)
{
    //inicio y fin de puntero
    char *s1 = s, *s2 = &s[strlen(s) - 1];

    //trim derecha
    while ((isspace(*s2)) && (s2 >= s1))
        s2--;
    *(s2 + 1) = '\0';

    //trim izquierda
    while ((isspace(*s1)) && (s1 < s2))
        s1++;

    //string limpio
    strcpy(s, s1);
    return s;
}


void parseConfig(struct parametros *params)
{
    char *s, buff[256];
    FILE *fp = fopen(CONFIG_FILE, "r");
    if (fp == NULL)
    {
        fp = fopen(CONFIG_FILE_2, "r");
        if(!fp)
        {
            return;
        }
    }

    //next line
    while ((s = fgets(buff, sizeof buff, fp)) != NULL)
    {
        //ignora saltos de linea y comentarios
        if (buff[0] == '\n' || buff[0] == '#')
            continue;

        //trabaja con el nombre / valor de la linea
        char nombre[MAXLEN], valor[MAXLEN];
        s = strtok(buff, "=");
        if (s == NULL)
            continue;
        else
            strncpy(nombre, s, MAXLEN);
        s = strtok(NULL, "=");
        if (s == NULL)
            continue;
        else
            strncpy(valor, s, MAXLEN);
        trim(valor);

        //completa el struct 
        if (strcmp(nombre, "IS_OK") == 0)
            strncpy(params->IS_OK, valor, MAXLEN);
        else if (strcmp(nombre, "NOTA") == 0)
            strncpy(params->NOTA, valor, MAXLEN);
        else if (strcmp(nombre, "CANTIDAD_ALUMNOS_GENERAR") == 0)
            strncpy(params->CANTIDAD_ALUMNOS_GENERAR, valor, MAXLEN);
        else if (strcmp(nombre, "CANTIDAD_MATERIAS_GENERAR") == 0)
            strncpy(params->CANTIDAD_MATERIAS_GENERAR, valor, MAXLEN);
        else if (strcmp(nombre, "NOMBRE_ALUMNO") == 0)
            strncpy(params->NOMBRE_ALUMNO, valor, MAXLEN);
        else if (strcmp(nombre, "APELLIDO_ALUMNO") == 0)
            strncpy(params->APELLIDO_ALUMNO, valor, MAXLEN);
        else if (strcmp(nombre, "NOMBRE_MATERIA") == 0)
            strncpy(params->NOMBRE_MATERIA, valor, MAXLEN);
        else
            printf("!!!!!: %s/%s: error con el nombre - valor!\n", nombre, valor);
    }
    //cierro configFile
    fclose(fp);
}

// int main()
// {
//     struct parametros params;
//     initParametros(&params);
//     parseConfig(&params);
//     printf("NOTA: %s \n", params.NOTA);
//     return 0;
// }