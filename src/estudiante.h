#include <string.h>
#include <time.h>
#include "materia-inscripta.h"

typedef struct Estudiante {
    // Puntero a siguiente estudiante (para lista)
    struct Estudiante *siguiente;

    char nombre[20];
    char apellido[20];
    int anioNacimiento, mesNacimiento, diaNacimiento;

    // Se genera con la insercion del estudiante y es unico.
    // (En el menu)
    int legajo;

    MateriaInscripta *materiasInscriptas;

} Estudiante;

Estudiante *crearNuevoEstudiante(char nombre[20], char apellido[20], int anioNacimiento,
                                int mesNacimiento, int diaNacimiento){
    Estudiante *nuevoEstudiante;
    nuevoEstudiante = malloc(sizeof(Estudiante));

    // Asignamos los datos al nuevo Estudiante
    strcpy(nuevoEstudiante->nombre, nombre);
    strcpy(nuevoEstudiante->apellido, apellido);
    nuevoEstudiante->anioNacimiento = anioNacimiento;
    nuevoEstudiante->mesNacimiento = mesNacimiento;
    nuevoEstudiante->diaNacimiento = diaNacimiento;

    nuevoEstudiante->siguiente = NULL;

    return nuevoEstudiante;
}

void agregarNuevaInscripcionALista(Estudiante *unEstudiante, int idMateria, char descripcion[25])
{
    
    MateriaInscripta *unaNuevaInscripcion = crearNuevaInscripcion(idMateria, descripcion);

    if (unEstudiante->materiasInscriptas == NULL)
    {
        unEstudiante->materiasInscriptas = unaNuevaInscripcion;
    }
    else
    {
        obtenerPosicionParaInsertarInscripcion(unEstudiante->materiasInscriptas)->siguiente = unaNuevaInscripcion;
    }
}

int compararCadenasPorApellidoYNombre(char nombre1[],char apellido1[],char nombre2[],char apellido2[]) {
    if(strcmp(apellido1,apellido2)>0)
        return 1;
    else {
        if (strcmp(apellido1, apellido2) == 0)
        {
            return strcmp(nombre1, nombre2);
        }
        else {
            return strcmp(apellido1, apellido2);
        }
    }
}

int calcularEdadDeEstudiante(int anioNacimiento)
{
    time_t t;
    struct tm *tm;
    t = time(NULL);
    tm = localtime(&t);
    return (1900 + tm->tm_year) - anioNacimiento;
}

void imprimirEdadDeEstudiante(Estudiante *estudiante)
{
    if (estudiante == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("Estudiante: Legajo: %d Apellido: %s. Nombre: %s. Edad: %d\n",
               estudiante->legajo, estudiante->apellido, estudiante->nombre, calcularEdadDeEstudiante(estudiante->anioNacimiento));
    }
}

double calcularPromedioDeNotasDeEstudiante(Estudiante *unEstudiante)
{
    if (unEstudiante->materiasInscriptas == NULL)
    {
        return 0;
    }
    else
    {
        MateriaInscripta *puntero = unEstudiante->materiasInscriptas;
        int cantidadDeMateriasRendidas = 0;
        int sumatoriaDeNotas = 0;

        while(puntero != NULL)
        {
            if(puntero->nota > 0)
            {
                cantidadDeMateriasRendidas++;
                sumatoriaDeNotas += puntero->nota;
            }
            puntero = puntero->siguiente;
        }

        if (cantidadDeMateriasRendidas == 0)
        {
            return 0;
        }
        else
        {
            double promedio = sumatoriaDeNotas / (double) cantidadDeMateriasRendidas;
            return promedio;
        }
    }
}
