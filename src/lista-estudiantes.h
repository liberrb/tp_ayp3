#include <stdio.h>
#include "estudiante.h"

Estudiante *listaEstudiantes = NULL;
int contadorLegajoEstudiantes = 10000;
int cantidadDeEstudiantes=0; //en vez de usar una funcion para calcular el largo de la lista
Estudiante *estudiantesOrdenadosPorEdad[5000];//vector de punteros

void insertarNuevoEstudianteALista(char nombre[20], char apellido[20], int anioNacimiento, int mesNacimiento, int diaNacimiento);
Estudiante *obtenerPosicionParaInsertarEstudiante(Estudiante *pEstudiante,char apellido[],char nombre[]);
void imprimirEstudiante(Estudiante *estudiante);
void imprimirListaDeEstudiantes(Estudiante *lista);
void eliminarLegajos(Estudiante *pEstudiante, int legajo);
int eliminarEstudiante(int legajo);
void buscarEstudiantesPorApellidoYNombre(Estudiante *lista);
void exportarEstudiantes(Estudiante *lista);
void exportarUnEstudiante(int legajo, int notaAprobacion);
char* validarAprobado(int notaEstudiante, int notaAprobacion);

void insertarNuevoEstudianteALista(char nombre[20], char apellido[20],
                                   int anioNacimiento, int mesNacimiento, int diaNacimiento)
{
    Estudiante *nuevoEstudiante = crearNuevoEstudiante(nombre, apellido, anioNacimiento, mesNacimiento, diaNacimiento);
    nuevoEstudiante->legajo = contadorLegajoEstudiantes++;
    nuevoEstudiante->materiasInscriptas = NULL;
    estudiantesOrdenadosPorEdad[cantidadDeEstudiantes]=nuevoEstudiante; // Vector de punteros
    cantidadDeEstudiantes++;


    if(listaEstudiantes == NULL)
    {
        listaEstudiantes = nuevoEstudiante;
    }
    else
    {

        if(compararCadenasPorApellidoYNombre(listaEstudiantes->nombre,listaEstudiantes->apellido,
                                             nuevoEstudiante->nombre,nuevoEstudiante->apellido) > 0)
        {
            Estudiante *nodoAux=listaEstudiantes;
            listaEstudiantes=nuevoEstudiante;
            listaEstudiantes->siguiente=nodoAux;
        }
        else {
            if(strcmp(listaEstudiantes->apellido,nuevoEstudiante->apellido)==0)
            {
                if(strcmp(listaEstudiantes->nombre,nuevoEstudiante->nombre)>0){
                    Estudiante *nodoAux=listaEstudiantes;
                    listaEstudiantes=nuevoEstudiante;
                    listaEstudiantes->siguiente=nodoAux;
                }
            }
            Estudiante *nodoPosicion = obtenerPosicionParaInsertarEstudiante(listaEstudiantes,apellido,nombre);
            Estudiante *nodoAux = nodoPosicion->siguiente;
            nodoPosicion->siguiente = nuevoEstudiante;
            nuevoEstudiante->siguiente= nodoAux;
        }
    }
}

Estudiante *obtenerPosicionParaInsertarEstudiante(Estudiante *pEstudiante,char apellido[],char nombre[])
{

    if(pEstudiante->siguiente == NULL || compararCadenasPorApellidoYNombre(pEstudiante->siguiente->nombre,
                                                                           pEstudiante->siguiente->apellido,
                                                                           nombre,apellido) > 0)
    {
        return pEstudiante;
    }
    return obtenerPosicionParaInsertarEstudiante(pEstudiante->siguiente, apellido,nombre);
}

void imprimirEstudiante(Estudiante *estudiante)
{
    if (estudiante == NULL)
    {
        printf("Estudiante inexistente.\n");
    }
    else
    {
        printf("\nEstudiante: Legajo: %d Apellido: %s. Nombre: %s. Fecha de nacimiento: %d/%d/%d.",
           estudiante->legajo, estudiante->apellido, estudiante->nombre, estudiante->anioNacimiento,
           estudiante->mesNacimiento, estudiante->diaNacimiento);
        if (estudiante->materiasInscriptas != NULL)
        {
            printf(" Promedio de notas: %.2f", calcularPromedioDeNotasDeEstudiante(estudiante));
        }
        printf("\n");
    }
}

void imprimirListaDeEstudiantes(Estudiante *lista)
{
    Estudiante *l = lista;

    if (l == NULL)
    {
        printf("Lista de Estudiantes vacia\n");
    }
    else
    {
        while (l != NULL)
        {
            imprimirEstudiante(l);
            l = l->siguiente;
        }
    }
}

Estudiante *obtenerEstudiantePorLegajo(int legajo)
{
    Estudiante *unEstudiante = listaEstudiantes;

    if (unEstudiante->legajo == legajo) {
        return unEstudiante;
    }
    else
    {
        while(unEstudiante->siguiente != NULL)
        {
            unEstudiante = unEstudiante->siguiente;
            if (unEstudiante->legajo == legajo){
                return unEstudiante;
            }
        }
    }
    printf("Estudiante no encontrado.\n");
    return NULL;
}

void limpiarMemoriaDeListaDeEstudiantes()
{
    Estudiante *sig;
    while (listaEstudiantes)
    {
        sig = listaEstudiantes->siguiente;
        free(listaEstudiantes);
        listaEstudiantes = sig;
    }

    listaEstudiantes = NULL;
    printf("\nMemoria de Lista de Estudiantes liberada.\n");
}

void eliminarLegajos(Estudiante *pEstudiante, int legajo)
{
    while (pEstudiante->siguiente != NULL && pEstudiante->siguiente->legajo == legajo)
    {
        Estudiante *temp = pEstudiante->siguiente;
        pEstudiante->siguiente = pEstudiante->siguiente->siguiente;
        free(temp);
    }
}

int eliminarEstudiante(int legajo)
{
    // Caso Lista vacia
    if (listaEstudiantes == NULL)
    {
        printf("La lista de estudiantes se encuentra vacia\n");
        return 0;
    }

    // Caso valor esta en la cabeza (unica vez o repetido varias veces)
    else if (listaEstudiantes->legajo == legajo)
    {
        while (listaEstudiantes->legajo == legajo)
        {
            Estudiante *aux = listaEstudiantes;

            // Si cabeza es el unico elemento en lista
            if (listaEstudiantes->siguiente == NULL)
            {
                free(aux);
                listaEstudiantes = NULL;
                break;
            }
            // Sino continuar
            else
            {
                listaEstudiantes = listaEstudiantes->siguiente;
                free(aux);
            }
        }
        cantidadDeEstudiantes--;
        return 1;
    }

    // Buscar el valor en los demas nodos
    else
    {
        Estudiante *puntero = listaEstudiantes;
        while (puntero != NULL && puntero->siguiente != NULL)
        {
            if (puntero->siguiente->legajo == legajo)
            {
                eliminarLegajos(puntero, legajo);
                cantidadDeEstudiantes--;
                return 1;
            }
            else
            {
                puntero = puntero->siguiente;
            }
        }
        printf("Legajo no encontrado\n");
        return 0;
    }
}

void buscarEstudiantesPorApellidoYNombre(Estudiante *lista)
{
    
    char nombre[20];
    char apellido[20];
    
    printf("Ingrese nombre: ");
    scanf("%s",nombre);
    printf("Ingrese apellido del alumno: ");
    scanf("%s",apellido);
    
    int encontrado = 0;

    Estudiante *pEstudiante;
    if (lista == NULL)
    {
        printf("Lista de Estudiantes Vacia!!!\n");
        return;
    }
    else
    {
        pEstudiante = lista;

        while (pEstudiante)
        {
            if (encontrado == 1)
            {
                if (!(strcmp(pEstudiante->apellido, apellido) == 0 && strcmp(pEstudiante->nombre, nombre) == 0))
                {
                    return;
                }
                imprimirEstudiante(pEstudiante);
            }
            else if (strcmp(pEstudiante->apellido, apellido) == 0 && strcmp(pEstudiante->nombre, nombre) == 0)
            {
                imprimirEstudiante(pEstudiante);
                encontrado = 1;
            }
            pEstudiante = pEstudiante->siguiente;
        }
    }

    if (encontrado == 0)
    {
        printf("\nEstudiante no encontrado\n");
    }
}

void exportarEstudiantes(Estudiante *lista)
{

    Estudiante *l = lista;

    if (l == NULL)
    {
        printf("Lista de Estudiantes Vacia\n");
    }
    else
    {
        FILE *fpt;
        fpt = fopen("Estudiantes.csv", "w+");
        fprintf(fpt, "Legajo, Apellido, Nombre, Fecha_Nac, Nota_Prom\n");
        while (l != NULL)
        {
            fprintf(fpt, "%d, %s, %s, %d/%d/%d, %.2f\n", l->legajo, l->apellido, l->nombre,
                    l->anioNacimiento, l->mesNacimiento, l->diaNacimiento,
                    calcularPromedioDeNotasDeEstudiante(l));
            l = l->siguiente;
        }
        fclose(fpt);
        printf("Archivo generado: Estudiantes.csv\n");
    }
}

void exportarUnEstudiante(int legajo, int notaAprobacion)
{

    Estudiante *unEstudiante;
    unEstudiante = obtenerEstudiantePorLegajo(legajo);

    if (unEstudiante != NULL)
    {

        char legajostr[10];
        sprintf(legajostr, "%d", legajo);
        char fileName[30] = "Estudiante_";
        strcat(fileName, legajostr);
        strcat(fileName, ".csv");

        FILE *fpt;
        fpt = fopen(fileName, "w+");
        fprintf(fpt, "Legajo, Apellido, Nombre, Fecha_Nac, Nota_Prom\n");
        fprintf(fpt, "%d, %s, %s, %d/%d/%d, %.2f\n", unEstudiante->legajo, unEstudiante->apellido, unEstudiante->nombre,
                unEstudiante->anioNacimiento, unEstudiante->mesNacimiento, unEstudiante->diaNacimiento,
                calcularPromedioDeNotasDeEstudiante(unEstudiante));

        if (unEstudiante->materiasInscriptas == NULL)
        {
            fprintf(fpt, "El estudiante no esta inscripto a ninguna materia.\n");
            fclose(fpt);
            return;
        }

        fprintf(fpt, "MateriaID, Descripcion, Nota, Estado\n");
        MateriaInscripta *l = unEstudiante->materiasInscriptas;
        while (l != NULL)
        {
            fprintf(fpt, "%d, %s, %d, %s\n", l->idMateria, l->descripcion, l->nota, validarAprobado(l->nota, notaAprobacion));
            l = l->siguiente;
        }
        fclose(fpt);
        printf("Archivo generado: %s\n", fileName);
    }
}

char* validarAprobado(int notaEstudiante, int notaAprobacion)
{

    if (notaEstudiante == 0)
    {
        return "Sin Rendir";
    }

    if (notaEstudiante >= notaAprobacion)
    {
        return "Aprobado";
    }

    return "Desaprobado";
}
