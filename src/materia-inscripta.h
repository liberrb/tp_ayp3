#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct MateriaInscripta
{
    int idMateria;
    int nota;
    char descripcion[25];

    struct MateriaInscripta *siguiente;
} MateriaInscripta;

MateriaInscripta *crearNuevaInscripcion(int idMateria, char descripcion[25])
{
    MateriaInscripta *nuevaInscripcion;
    nuevaInscripcion = malloc(sizeof (MateriaInscripta));
    nuevaInscripcion->idMateria = idMateria;
    nuevaInscripcion->nota = 0;
    strcpy(nuevaInscripcion->descripcion, descripcion);

    nuevaInscripcion->siguiente = NULL;

    return nuevaInscripcion;
}

MateriaInscripta *obtenerPosicionParaInsertarInscripcion(MateriaInscripta *pInscripcion)
{
    if (pInscripcion->siguiente == NULL)
    {
        return pInscripcion;
    }
    return obtenerPosicionParaInsertarInscripcion(pInscripcion->siguiente);
}

void imprimirMateriaInscripta(MateriaInscripta *pInscripcion)
{
    if (pInscripcion == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("\nMateria - ID: %d, Descripcion: %s, Nota: ", pInscripcion->idMateria, pInscripcion->descripcion);

        if(pInscripcion->nota == 0)
        {
            printf("Sin rendir.\n");
        }
        else
        {
            printf("%d\n", pInscripcion->nota);
        }
    }
}

void imprimirListaDeMateriasInscriptas(MateriaInscripta *lista)
{
    MateriaInscripta *l = lista;

    if (l == NULL)
    {
        printf("Lista de materias inscriptas vacia\n");
    }
    else
    {
        printf("Inscripto en las siguientes materias:\n");
        while (l != NULL)
        {
            imprimirMateriaInscripta(l);
            l = l->siguiente;
        }
    }
}

MateriaInscripta *obtenerInscripcionPorIDDeMateria(MateriaInscripta *listaInscriptas, int idMateria)
{
    MateriaInscripta *unaInscripcion = listaInscriptas;

    if (unaInscripcion->idMateria == idMateria) {
        return unaInscripcion;
    }
    else
    {
        unaInscripcion = unaInscripcion->siguiente;
        while(unaInscripcion->siguiente != NULL)
        {
            if (unaInscripcion->idMateria == idMateria){
                return unaInscripcion;
            }
            unaInscripcion = unaInscripcion->siguiente;
        }
    }

    if (unaInscripcion->idMateria == idMateria){
        return unaInscripcion;
    }

    return NULL;
}



