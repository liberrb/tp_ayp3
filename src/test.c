#include <time.h>
#include <stdbool.h>
#include "lista-estudiantes.h"
#include "lista-materias.h"
#include "config.h"

bool testAgregarUnEstudiante()
{
    insertarNuevoEstudianteALista("Jose", "San Martin", 1778, 2, 25);
    return cantidadDeEstudiantes == 1;
}

bool testAgregarEstudiantes()
{
    insertarNuevoEstudianteALista("Manuel", "Alberti", 1763, 5, 28);
    insertarNuevoEstudianteALista("Manuel", "Belgrano", 1770, 6, 3);
    insertarNuevoEstudianteALista("Mariano", "Moreno", 1778, 9, 23);
    return cantidadDeEstudiantes == 4;
}

bool testAgregarUnaMateria()
{
    insertarNuevaMateriaALista("Algebra I");
    int cantidadMat = obtenerCantidadDeMaterias();
    return cantidadMat == 1;
}

bool testAgregarMaterias()
{
    insertarNuevaMateriaALista("Algoritmos y Programacion III");
    insertarNuevaMateriaALista("Estructura de Datos");
    insertarNuevaMateriaALista("Matematicas Discretas");
    int cantidadMat = obtenerCantidadDeMaterias();
    return cantidadMat == 4;
}

bool testAnotarAlumnoAMateria(int legajo, int materiaID)
{

    Estudiante *unEstudiante;
    Materia *unaMateria;
    unEstudiante = obtenerEstudiantePorLegajo(legajo);
    if (unEstudiante == NULL)
    {
        return false;
    }

    unaMateria = obtenerMateriaPorID(materiaID);
    if (unaMateria == NULL)
    {
        return false;
    }
    agregarNuevaInscripcionALista(unEstudiante, materiaID, unaMateria->nombre);
    return unEstudiante->materiasInscriptas != NULL;
}

bool testRendirUnaMateria(int legajo, int materiaID, int nota)
{
    Estudiante *unEstudiante;
    Materia *unaMateria;
    unEstudiante = obtenerEstudiantePorLegajo(legajo);
    if (unEstudiante == NULL)
    {
        return false;
    }

    if (unEstudiante->materiasInscriptas == NULL)
    {
        return false;
    }

    MateriaInscripta *unaMateriaInscripta = obtenerInscripcionPorIDDeMateria(unEstudiante->materiasInscriptas, materiaID);

    if (unaMateriaInscripta == NULL)
    {
        return false;
    }

    unaMateriaInscripta->nota = nota; // asigna nota al estudiante

    return unaMateriaInscripta->nota == nota;
}

bool testEliminarEstudianteDeLaLista(int legajo)
{
    int tempCantindadEstudiantes = cantidadDeEstudiantes;
    if (!eliminarEstudiante(legajo))
    {
        return false;
    }
    return tempCantindadEstudiantes > cantidadDeEstudiantes;
}

bool testExportarListaEstudiantes()
{
    exportarEstudiantes(listaEstudiantes);
    FILE *fpt;
    fpt = fopen("Estudiantes.csv", "r");
    if (!fpt)
    {
        return false;
    }
    fclose(fpt);
    return true;
}

bool testExportarDetalleDeUnEstudiantes(int legajo)
{
    exportarUnEstudiante(legajo, 6);

    char legajostr[10];
    sprintf(legajostr, "%d", legajo);
    char fileName[30] = "Estudiante_";
    strcat(fileName, legajostr);
    strcat(fileName, ".csv");

    FILE *fpt;
    fpt = fopen(fileName, "r");
    if (!fpt)
    {
        return false;
    }
    fclose(fpt);
    return true;
}

bool testInicializarArchivoConfig()
{
    struct parametros params;
    initParametros(&params);
    parseConfig(&params);
    return atoi(params.IS_OK) == 1;
}

bool testCalcularPromedio(int legajo)
{

    testAnotarAlumnoAMateria(legajo, 1);
    testAnotarAlumnoAMateria(legajo, 2);
    testRendirUnaMateria(legajo, 1, 7);
    testRendirUnaMateria(legajo, 2, 6);

    Estudiante *unEstudiante;
    unEstudiante = obtenerEstudiantePorLegajo(legajo);
    if (unEstudiante == NULL)
    {
        return false;
    }

    double esperado = 6.50;
    double promedio = calcularPromedioDeNotasDeEstudiante(unEstudiante);
    return esperado == promedio;
}

int main()
{
    printf("\n");
    printf("#### Running Tests ####\n\n");

    printf("TestAgregarUnEstudiante: %s \n", testAgregarUnEstudiante() ? "true" : "false");
    printf("TestAgregarEstudiantes: %s \n", testAgregarEstudiantes() ? "true" : "false");
    printf("TestAgregarUnaMateria: %s \n", testAgregarUnaMateria() ? "true" : "false");
    printf("TestAgregarMaterias: %s \n", testAgregarMaterias() ? "true" : "false");
    printf("TestAnotarAlumnoAMateria 1: %s \n", testAnotarAlumnoAMateria(10002, 2) ? "true" : "false");
    printf("TestAnotarAlumnoAMateria 2: %s \n", testAnotarAlumnoAMateria(10002, 1) ? "true" : "false");
    printf("TestAnotarAlumnoAMateria 3: %s \n", testAnotarAlumnoAMateria(10002, 3) ? "true" : "false");
    printf("TestAnotarAlumnoAMateriaQueNoExiste: %s \n", !testAnotarAlumnoAMateria(10002, 300) ? "true" : "false");
    printf("TestAnotarAlumnoQueNoExisteAMateria: %s \n", !testAnotarAlumnoAMateria(9999, 2) ? "true" : "false");
    printf("TestRendirUnaMateria: %s \n", testRendirUnaMateria(10002, 3, 8) ? "true" : "false");
    printf("TestRendirUnaMateriaQueNoEstaAnotado: %s \n", !testRendirUnaMateria(10002, 4, 2) ? "true" : "false");
    printf("TestEliminarEstudianteDeLaLista: %s \n", testEliminarEstudianteDeLaLista(10003) ? "true" : "false");
    printf("TestEliminarEstudianteQueNoExisteDeLaLista: %s \n", !testEliminarEstudianteDeLaLista(999) ? "true" : "false");
    printf("TestExportarListaEstudiantes: %s \n", testExportarListaEstudiantes() ? "true" : "false");
    printf("TestExportarDetalleDeUnEstudiantes: %s \n", testExportarDetalleDeUnEstudiantes(10002) ? "true" : "false");
    printf("TestInicializarArchivoConfig: %s \n", testInicializarArchivoConfig() ? "true" : "false");
    printf("TestCalcularPromedio: %s \n", testCalcularPromedio(10001) ? "true" : "false");
    printf("\n");
    return 0;
}
