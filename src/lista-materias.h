#include <stdio.h>
#include "materia.h"

Materia *listaMaterias = NULL;
int contadorIDMaterias = 0;

void insertarNuevaMateriaALista(char nombre[25]);
Materia *obtenerPosicionParaInsertarMateria(Materia *pMateria);
void imprimirMateria(Materia *pMateria);
void imprimirListaDeMaterias(Materia *lista);

void insertarNuevaMateriaALista(char nombre[25])
{
    Materia *nuevaMateria = crearNuevaMateria(nombre);
    nuevaMateria->idMateria = ++contadorIDMaterias;

    if(listaMaterias == NULL)
    {
        listaMaterias = nuevaMateria;
    }
    else
    {
        obtenerPosicionParaInsertarMateria(listaMaterias)->siguiente = nuevaMateria;
    }
}

Materia *obtenerPosicionParaInsertarMateria(Materia *pMateria)
{
    if (pMateria->siguiente == NULL)
    {
        return pMateria;
    }
    return obtenerPosicionParaInsertarMateria(pMateria->siguiente);
}

Materia *obtenerMateriaPorID(int idMateria)
{
    Materia *unaMateria = listaMaterias;

    if (unaMateria->idMateria == idMateria) {
        return unaMateria;
    }
    else
    {
        while(unaMateria->siguiente != NULL)
        {
            unaMateria = unaMateria->siguiente;
            if (unaMateria->idMateria == idMateria){
                return unaMateria;
            }
        }
    }
    return NULL;
}

void imprimirMateria(Materia *pMateria)
{
    if (pMateria == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        printf("\nMateria: ID: %d, Nombre: %s\n", pMateria->idMateria, pMateria->nombre);
    }
}

void imprimirListaDeMaterias(Materia *lista)
{
    Materia *l = lista;

    if (l == NULL)
    {
        printf("Lista de Materias vacia\n");
    }
    else
    {
        while (l != NULL)
        {
            imprimirMateria(l);
            l = l->siguiente;
        }
    }
}

int obtenerCantidadDeMaterias()
{
    return contadorIDMaterias;
}

void limpiarMemoriaDeListaDeMaterias()
{
    Materia *sig;
    while (listaMaterias)
    {
        sig = listaMaterias->siguiente;
        free(listaMaterias);
        listaMaterias = sig;
    }

    listaMaterias = NULL;
    printf("Memoria de Lista de Materias liberada.\n");
}