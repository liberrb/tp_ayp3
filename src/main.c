#include <time.h>
#include <stdbool.h>
#include "lista-estudiantes.h"
#include "lista-materias.h"
#include "config.h"

// Metodos de validacion
struct parametros params;
void clean_stdin();
int validarTipoDatoInt();
int validarEdad();
bool validarFecha(int anio, int mes, int dia);
void solicitarStringDeLongitud(char string[], int longitud);
void eliminarSaltoDeLineaEnString(char string[]);
void iniciarConfig();
int randomInt(int minimo, int maximo);
Estudiante *solicitarEstudiantePorLegajo();

// Metodos opciones del menu
void generarAlumnos();
void generarMaterias();
void agregarNuevoEstudiante();
void agregarNuevaMateria();
void asignarEstudianteAMateria();
void rendirMateriaComoEstudiante();
void eliminarEstudianteDeLista();
void ordenarEstudiantesPorEdad(Estudiante *estudiantesOrdenadosPorEdad[], int izq, int der);
void imprimirEstudiantesPorRangoDeEdad(int edadMinima, int edadMaxima);
void exportarEstudiante();
void buscarEstudiantesPorRangoDeEdad();

int main()
{
    iniciarConfig();
    int opcion = 9999;
    int generado = 0;

    while (opcion != 0)
    {
        printf("\nMENU:\n");
        printf("1. Crear/Iniciar Listas\n");
        printf("2. Agregar nuevo Estudiante\n");
        printf("3. Agregar nueva Materia\n");
        printf("4. Anotar un Estudiante a una Materia\n");
        printf("5. Rendir una Materia como Estudiante\n");
        printf("6. Eliminar un Estudiante de la lista\n");
        printf("7. Imprimir lista de Estudiantes\n");
        printf("8. Imprimir lista de Materias\n");
        printf("9. Buscar estudiante por nombre y apellido\n");
        printf("10. Buscar estudiantes por rango de edad\n");
        printf("11. Exportar lista de Estudiantes\n");
        printf("12. Exportar detalle de Estudiante\n");
        printf("0. Salir\n");
        printf("\nSeleccione una opcion: ");
        opcion = validarTipoDatoInt();

        switch (opcion)
        {
        case 1:
            // Lee los datos del config y genera alumnos
            // utilizo el flag para no generar cada vez que se selecciona la opcion, es para iniciar la lista (es decir para unico uso)
            if (!generado)
            {
                generarAlumnos();
                generarMaterias();
                generado = 1;
                printf("INCIALIZACION COMPLETADA\n");
            }
            else
            {
                printf("LA INCICIALIZACION YA HABIA SIDO EJECUTADA\n");
            }
            break;
        case 2:
            agregarNuevoEstudiante();
            break;

        case 3:
            agregarNuevaMateria();
            break;

        case 4:
            asignarEstudianteAMateria();
            break;

        case 5:
            rendirMateriaComoEstudiante();
            break;

        case 6:
            eliminarEstudianteDeLista();
            break;

        case 7:
            imprimirListaDeEstudiantes(listaEstudiantes);
            break;

        case 8:
            imprimirListaDeMaterias(listaMaterias);
            break;

        case 9:
            buscarEstudiantesPorApellidoYNombre(listaEstudiantes);
            break;

        case 10:
            buscarEstudiantesPorRangoDeEdad();
            break;

        case 11:
            exportarEstudiantes(listaEstudiantes);
            break;

        case 12:
            exportarEstudiante();
            break;

        case 0:
            limpiarMemoriaDeListaDeEstudiantes();
            limpiarMemoriaDeListaDeMaterias();

            #if _WIN32
                system("cls");
            #else
                system("clear");
            #endif

            break;

        default:
            printf("\n###########\n");
            printf("Numero de opcion incorrecto\n");
            printf("###########\n");
            break;
        }
    }
}

int validarTipoDatoInt()
{
    int opcion;
    while(scanf("%d",&opcion) != 1)
    {
        printf("Solo se aceptan numeros enteros, intentemos otra vez: ");
        while(getchar() != '\n');
    }
    clean_stdin();
    return opcion;
}

void eliminarSaltoDeLineaEnString(char string[]){
    char *ptr = strchr(string, '\n');
    if (ptr)
    {
        *ptr  = '\0';
    }
}

void clean_stdin()
{
    #if _WIN32
        fflush(stdin);
    #else
        int c;
        do
        {
            c = getchar();
        } while (c != '\n' && c != EOF);
    #endif
}

void solicitarStringDeLongitud(char string[], int longitud)
{
    fgets(string, longitud, stdin);
    eliminarSaltoDeLineaEnString(string);
    clean_stdin();   
}

Estudiante *solicitarEstudiantePorLegajo()
{
    int legajo;
    printf("Ingrese el legajo del estudiante: ");
    legajo = validarTipoDatoInt();

    return obtenerEstudiantePorLegajo(legajo);
}

void iniciarConfig()
{
    initParametros(&params);
    parseConfig(&params);
    if (atoi(params.IS_OK) == 1)
    {
        printf("ARCHIVO CONFIG LEIDO CON EXITO");
    }
    else
    {
        printf("ARCHIVO CONFIG NO LEIDO ESTA OPERANDO CON VALORES DEFAULT");
    }
}

void agregarNuevoEstudiante()
{
    char nombre[20];
    char apellido[20];
    
    printf("Ingrese el nombre del estudiante (max. 20 caracteres):");
    solicitarStringDeLongitud(nombre, 20);

    printf("Ingrese el apellido del estudiante (max. 20 caracteres): ");
    solicitarStringDeLongitud(apellido, 20);

    int anioNacimiento, mesNacimiento, diaNacimiento;
    do
    {
        printf("Se solicita la fecha de nacimiento del estudiante en formato aaaa/mm/dd: ");
        while(scanf("%d/%d/%d", &anioNacimiento,&mesNacimiento, &diaNacimiento) != 3)
        {
            printf("Formato de fecha invalido. Intente nuevamente: ");
            clean_stdin();
        }
    }
    while (!validarFecha(anioNacimiento, mesNacimiento, diaNacimiento));

    insertarNuevoEstudianteALista(nombre, apellido, anioNacimiento, mesNacimiento, diaNacimiento);
}

bool validarFecha(int anio, int mes, int dia)
{
    // Validar anio
    if(anio>=1900 && anio<=9999)
    {
        // Validar mes
        if(mes>=1 && mes<=12)
        {
            // Validar dia
            if
                // Meses 31 dias
                    (((dia>=1 && dia<=31) && (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12))
                     // Meses 30 dias
                     || ((dia>=1 && dia<=30) && (mes==4 || mes==6 || mes==9 || mes==11))
                     // Mes febrero 28 dias
                     || ((dia>=1 && dia<=28) && (mes==2))
                     // Mes febrero 29 dias (anio bisiesto)
                     || (dia==29 && mes==2 && (anio%400==0 ||(anio%4==0 && anio%100!=0)))
                    )
                return true;
            else
                printf("El dia ingresado no es valido. Intente nuevamente.\n");
            return false;
        }
        else
        {
            printf("El mes ingresado no es valido. Intente nuevamente.\n");
            return false;
        }
    }
    else
    {
        printf("El anio ingresado no es valido. Intente nuevamente.\n");
        return false;
    }
}

void agregarNuevaMateria()
{
    char nombreMateria[25];
    printf("Ingrese el nombre de la materia (max. 25 caracteres): ");
    solicitarStringDeLongitud(nombreMateria, 25);
    insertarNuevaMateriaALista(nombreMateria);
}

void asignarEstudianteAMateria()
{
    if(listaMaterias == NULL)
    {
        printf("No hay materias en la lista.\n");
    }
    else {
        Estudiante *unEstudiante = solicitarEstudiantePorLegajo();
        if (unEstudiante != NULL) {
            int opcionMateria;

            imprimirListaDeMaterias(listaMaterias);

            printf("\n Seleccione una materia: ");
            opcionMateria = validarTipoDatoInt();

            Materia *unaMateria;
            unaMateria = obtenerMateriaPorID(opcionMateria);
            if (unaMateria != NULL)
            {
                agregarNuevaInscripcionALista(unEstudiante, opcionMateria, unaMateria->nombre);
                imprimirListaDeMateriasInscriptas(unEstudiante->materiasInscriptas);
            }
            else
            {
                printf("Materia no encontrada.\n");
                return;
            }
        }
        else
        {
            printf("No existe estudiante con ese legajo.\n");
            return;
        }
    }
}

void rendirMateriaComoEstudiante()
{
    Estudiante *unEstudiante = solicitarEstudiantePorLegajo();

    if (unEstudiante == NULL)
    {
        return;
    }

    if(unEstudiante->materiasInscriptas == NULL)
    {
        printf("El estudiante no esta inscripto a ninguna materia.\n");
        return;
    }

    int opcionMateria;
    imprimirListaDeMateriasInscriptas(unEstudiante->materiasInscriptas);

    printf("\n Seleccione una materia: ");
    opcionMateria = validarTipoDatoInt();

    MateriaInscripta *unaMateriaInscripta =
            obtenerInscripcionPorIDDeMateria(unEstudiante->materiasInscriptas, opcionMateria);

    if ( unaMateriaInscripta != NULL)
    {
        printf("Se selecciono la materia: ");
        imprimirMateriaInscripta(unaMateriaInscripta);

        int notaRendida = 0;

        printf("Ingrese la nota:");

        notaRendida = validarTipoDatoInt();

        unaMateriaInscripta->nota = notaRendida;
    }
    else
    {
        printf("La materia ingresada no existe.\n");
    }
}

int randomInt(int minimo, int maximo)
{
    // para moverme entre los indices y no pasarme
    int num = (rand() % (maximo - minimo + 1)) + minimo;
    return num;
}

void generarAlumnos()
{
    srand(time(0));
    const char s[4] = "_";
    int indice = 0;
    int minimo = 0;

    // seteo valores para nombre desde nombres del archivo config
    int maximoNom = 7;
    char *tokenNom = strtok(params.NOMBRE_ALUMNO, s);

    char arrayNom[maximoNom][20];
    while (tokenNom != 0 || indice < maximoNom)
    {
        // printf(" %s\n", tokenNom);
        strcpy(arrayNom[indice], tokenNom);
        indice++;
        tokenNom = strtok(0, s);
    }

    // seteo valores para apellidos desde apellido del archivo config
    indice = 0;
    int maximoApe = 7;
    char *tokenApe = strtok(params.APELLIDO_ALUMNO, s);

    char arrayApe[maximoApe][20];
    while (tokenApe != 0 || indice < maximoApe)
    {
        // printf(" %s\n", tokenApe);
        strcpy(arrayApe[indice], tokenApe);
        indice++;
        tokenApe = strtok(0, s);
    }

    // los inserto en la lista principal
    for (int i = 0; i < atoi(params.CANTIDAD_ALUMNOS_GENERAR); i++)
    {
        insertarNuevoEstudianteALista(arrayNom[randomInt(minimo, maximoNom)], arrayApe[randomInt(minimo, maximoApe)], randomInt(1980, 2002), randomInt(1, 12), randomInt(1, 28));
    }
}

void generarMaterias()
{
    srand(time(0));
    int indice = 0;
    int maximo = 6;
    const char s[4] = "_";
    char *tokenMateria = strtok(params.NOMBRE_MATERIA, s);

    char arrayMateria[maximo][25];
    while (tokenMateria != 0 || indice < maximo)
    {
        strcpy(arrayMateria[indice], tokenMateria);
        indice++;
        tokenMateria = strtok(0, s);
    }

    for (int i = 0; i < atoi(params.CANTIDAD_MATERIAS_GENERAR); i++)
    {
        insertarNuevaMateriaALista( arrayMateria[randomInt(0, maximo)] );
    }
}

void eliminarEstudianteDeLista()
{
    int legajo;
    printf("Ingrese el legajo del estudiante: ");
    legajo = validarTipoDatoInt();
    int result = eliminarEstudiante(legajo);
    if (result == 1)
    {
        printf("El estudiante con legajo %d fue eliminado con exito\n", legajo);
    }
}

void ordenarEstudiantesPorEdad(Estudiante *estudiantesOrdenadosPorEdad[], int izq, int der){
    int i,j,x;
    Estudiante *w;
    
    i=izq;
    j=der;
    x=calcularEdadDeEstudiante(estudiantesOrdenadosPorEdad[(izq+der)/2]->anioNacimiento);
    do{
        while((calcularEdadDeEstudiante(estudiantesOrdenadosPorEdad[i]->anioNacimiento))<x)
            i++;
        while(x<(calcularEdadDeEstudiante(estudiantesOrdenadosPorEdad[j]->anioNacimiento)))
            j--;
        if(i<=j){
            w=estudiantesOrdenadosPorEdad[i];
            estudiantesOrdenadosPorEdad[i]=estudiantesOrdenadosPorEdad[j];
            estudiantesOrdenadosPorEdad[j]=w;
            i++;
            j--;
        }// cierro if
    }//cierro do
    while(i<=j);
    if(izq<j)
        ordenarEstudiantesPorEdad(estudiantesOrdenadosPorEdad,izq,j);
    if(i<der)
        ordenarEstudiantesPorEdad(estudiantesOrdenadosPorEdad,i,der);
};

void imprimirEstudiantesPorRangoDeEdad(int edadMinima, int edadMaxima)
{
    int cantidad=0;
    for (int i = 0; i < cantidadDeEstudiantes; i++)
    {
        if ((calcularEdadDeEstudiante(estudiantesOrdenadosPorEdad[i]->anioNacimiento)) >= edadMinima && (calcularEdadDeEstudiante(estudiantesOrdenadosPorEdad[i]->anioNacimiento)) <= edadMaxima)
        {
            imprimirEdadDeEstudiante(estudiantesOrdenadosPorEdad[i]);
            cantidad++;
        }
    }
    if(cantidad==0)
        printf("\tNo hay estudiantes dentro del rango elegido.\n");
}

void exportarEstudiante()
{
    int legajo;
    printf("Ingrese el legajo del estudiante: ");
    legajo = validarTipoDatoInt();
    exportarUnEstudiante(legajo, atoi(params.NOTA));
}

int validarEdad()
{
    int edad;
    edad = validarTipoDatoInt(); // verifique que es un numero
    if (edad < 18 || edad > 70)
    {
        printf("Edad invalida. Intente nuevamente. Ingrese un numero entero entre 18 y 70:");
        edad = validarEdad();
    }
    return edad;
}

void buscarEstudiantesPorRangoDeEdad()
{
    int edadMinima, edadMaxima, aux;

    if (cantidadDeEstudiantes >= 2)
    {
        ordenarEstudiantesPorEdad(estudiantesOrdenadosPorEdad,0, cantidadDeEstudiantes-1);
    }

    printf("Ingrese el valor minimo de edad a buscar:");
    edadMinima = validarEdad();
    printf("Ingrese el valor Maximo de edad a buscar:");
    edadMaxima = validarEdad();

    if (edadMinima > edadMaxima)
    {
        aux = edadMaxima;
        edadMaxima = edadMinima;
        edadMinima = aux;
    }

    printf("\tEstudiantes entre %d y %d anios de edad.\n", edadMinima, edadMaxima);
    imprimirEstudiantesPorRangoDeEdad(edadMinima, edadMaxima);
}
