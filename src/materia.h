#include <string.h>
#include <stdlib.h>

typedef struct Materia
{
    struct Materia *siguiente;
    char nombre[25];
    int idMateria;
} Materia;

Materia *crearNuevaMateria(char nombre[25])
{
    Materia *nuevaMateria;
    nuevaMateria = malloc(sizeof (Materia));

    strcpy(nuevaMateria->nombre, nombre);

    nuevaMateria->siguiente = NULL;

    return nuevaMateria;
}